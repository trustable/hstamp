# hstamp

This is an experimental analysis and simulation tool for STPA. Please refer to
the user manual in [html](http://trustable.gitlab.io/hstamp) or
[pdf](https://trustable.gitlab.io/hstamp/book.pdf) format for operation.

htstamp uses [RIO](https://hackage.haskell.org/package/rio) prelude, the
[etc](https://hackage.haskell.org/package/etc) configuration manager and
[gt-gtk-declarative](https://hackage.haskell.org/package/gi-gtk-declarative).
This builds in gitlab's ci to produce an output binary.

## Prerequisites

You will need gtk development libraries:

Ubuntu:

    sudo apt-get install pkg-config gtk-3.0 gobject-introspection libgirepository1.0-dev

Arch Linux:

    sudo pacman -S gobject-introspection gtk3

## Building

Build with

    stack build

Run with

    stack exec -- hstamp run


