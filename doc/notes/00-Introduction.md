# Introduction

hstamp is a capture, simulation and analysis tool for systems-theoretic process
analysis.

It aims to provide a coherent framework and domain specific language for modelling
control systems.

It doesn't exist yet, it's just a generic gtk application based the
[rio-etc-gtk-declarative-template](https://gitlab.com/zenhaskell/rio-etc-gtk-declarative-template)

If it did exist, this book would be the manual.

## Building from Source

You can build from source and test iteratively using
[stack](https://docs.haskellstack.org/en/stable/README/)

    stack build
    stack exec -- hstamp run

You can install the application for your current user with

    stack install

Then run

    hstamp run

You can dump the config with

    hstamp config

# Technical Notes

The following is based on a generic approach to signal processing taken from
the paper [Functional Reactive Programming,
Refactored](http://www.cs.nott.ac.uk/~psxip1/papers/2016-HaskellSymposium-Perez-Barenz-Nilsson-FRPRefactored-short.pdf).
We show some simple worked examples to illustrate the approach, using
[dunai](https://github.com/ivanperez-keera/dunai) as-is. We combine this with
the [dimensional](http://hackage.haskell.org/package/dimensional) library to
ensure physical coherence of the systems produced.

We also reference [Seven Sketches in
Compositionality](https://arxiv.org/pdf/1803.05316.pdf) for the semantic
interpretation.

## The Goal

We aim to establish a EDSL capable of expressing the diagram models described
in STPA. It should be expressive enough to capture all important coherence
properties of the signals, whilst accessible enough such that information can
be supplied via a GUI. We also wish to provide options for injecting values in
the system to vary simulation properties, run subsystems in isolation and
compose independently constructed systems.

An STPA diagram, broadly, is a monoidal category interpreted as a [string
diagram](https://ncatlab.org/nlab/show/string+diagram). The 'objects' of this
category are the signals (STPA 'arrows'). The arrows in this category are the
signal processors (STPA 'boxes'), that accept signals and provide output
signals.

In some cases, signal processors are merely functions, accepting input signal
and transducing it into a response. In other cases, arbitrary computational
effects may be required. For example tracking the state of an individual or
object, observing an internal clock. dunai allows us to express signal
functions as monadic arrows, with a fair degree of flexibility and arbitrarity.
(TODO: describe this better).

## Some Worked Examples

The examples here can be run directly as standalone scripts.

```{.haskell}
#!/usr/bin/env stack
-- stack runhaskell --resolver lts-12.12 --package rio --package dunai --package dimensional

{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE RankNTypes        #-}

import           Control.Monad.Trans.MSF
import           Data.MonadicStreamFunction
import           Numeric.Units.Dimensional.Prelude
import           RIO                     hiding ( (/)
                                                , (*)
                                                , (+)
                                                )
import           RIO.State               hiding ( get )

type DriverT m = forall a. Fractional a => MSF (StateT (CaffieneLevels a) m) () (Force a)

caffieneCurve :: Fractional a => CaffieneLevels a -> Force a
caffieneCurve x = (x /~ (mole / litre)) *~ newton

driver :: Monad m => DriverT m
driver = arrM $ const $ do
  modify (+ 1 *~ (mole / litre))
  caffieneCurve <$> get

system :: (Fractional a, Monad m) => MSF m () (Force a)
system = (runStateS__ driver (5 *~ (mole / litre)))

type CaffieneLevels a = AmountOfSubstanceConcentration a

main = runSimpleApp $ do
  x <- embed system [(), ()]
  logInfo $ displayShow x
```


### Simulating an elevator

TODO

### Simulating hydraulic brakes with a driver

TODO
