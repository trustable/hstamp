#!/usr/bin/env stack
-- stack runhaskell --resolver lts-12.12 --package rio --package dunai --package dimensional

{-# LANGUAGE Arrows            #-}
{-# LANGUAGE GADTs             #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE RankNTypes        #-}

import           Control.Monad.Trans.MSF
import           Data.MonadicStreamFunction hiding (second)
import           Numeric.Units.Dimensional.Prelude hiding ((.))
import           RIO                     hiding ( (/)
                                                , (*)
                                                , (+)
                                                , second)
import           RIO.State               hiding ( get )

data Toggle = Disable | Enable
  deriving (Eq, Ord, Show)

data Toggled = Disabled | Enabled
  deriving (Eq, Ord, Show)

reflectToggle :: Toggle -> Toggled
reflectToggle Disable = Disabled
reflectToggle Enable = Enabled

newtype AutoHoldControlSignal = AutoHoldControlSignal { autoHoldToggle :: Toggle }
  deriving (Eq, Ord, Show)

newtype AutoHoldFeedbackSignal = AutoHoldFeedbackSignal { autoHoldToggled :: Toggled }
  deriving (Eq, Ord, Show)

newtype BrakePedalControlSignal = BrakePedalControlSignal { brakePedalToggle :: Toggle }
  deriving (Eq, Ord, Show)

data PressureControlSignal where
  HoldPressure       :: PressureControlSignal
  ReleasePressure    :: PressureControlSignal
  AdditionalPressure :: PressureControlSignal
  deriving (Eq, Show)

type AutoHoldModuleInputs a = (AutoHoldControlSignal, BrakePedalControlSignal, TyrePressure a)
type AutoHoldModuleOutputs a = (PressureControlSignal, AutoHoldFeedbackSignal)

type AutoHoldModule m a = MSF m (AutoHoldModuleInputs a) (AutoHoldModuleOutputs a)

newtype TyrePressure a = TyrePressure (Pressure a)

ahmcf :: (Fractional a, Monad m) => AutoHoldModule m a
ahmcf = proc (AutoHoldControlSignal a, BrakePedalControlSignal b, c) ->
   returnA -< (HoldPressure, AutoHoldFeedbackSignal (reflectToggle a))

main = runSimpleApp $ do
  let initialPressure = TyrePressure (5 *~ (kilo gram / (metre * second * second)))
  xs <- embed ahmcf [(AutoHoldControlSignal Enable, BrakePedalControlSignal Enable, initialPressure)]
  logInfo . displayShow $ xs
