#!/usr/bin/env stack
-- stack runhaskell --resolver lts-12.12 --package rio --package dunai --package dimensional

{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE RankNTypes        #-}

import           Control.Monad.Trans.MSF
import           Data.MonadicStreamFunction
import           Numeric.Units.Dimensional.Prelude
import           RIO                     hiding ( (/)
                                                , (*)
                                                , (+)
                                                )
import           RIO.State               hiding ( get )

type DriverT m = forall a. Fractional a => MSF (StateT (CaffieneLevels a) m) () (Force a)

caffieneCurve :: Fractional a => CaffieneLevels a -> Force a
caffieneCurve x = (x /~ (mole / litre)) *~ newton

driver :: Monad m => DriverT m
driver = arrM $ const $ do
  modify (+ 1 *~ (mole / litre))
  caffieneCurve <$> get

system :: (Fractional a, Monad m) => MSF m () (Force a)
system = (runStateS__ driver (5 *~ (mole / litre)))

type CaffieneLevels a = AmountOfSubstanceConcentration a

main = runSimpleApp $ do
  x <- embed system [(), ()]
  logInfo $ displayShow x
